import re
from typing import Callable, Generator, Any
from collections import namedtuple


Pattern = namedtuple('Pattern', ['priority', 'regex', 'function'])


class Tokenizer:
    def __init__(self):
        self._patterns = []

    def pattern(self, regex: str, priority: int = None) -> None:
        """Register function to be called on matching pattern for token."""
        
        # If no specific priority is provided,
        # it will be prioritized below all patterns so far defined
        if priority is None:
            priority = min([1] + [p.priority for p in self._patterns]) - 1

        def add_pattern(f: Callable):
            self._patterns.append(Pattern(priority, re.compile(regex), f))

            # Sort all patterns by priority
            self._patterns = sorted(
                self._patterns,
                key=lambda p: p.priority,
                reverse=True
            )
            return f

        return add_pattern

    def match(self, text: str, handle_error: str = None) -> Generator[Any, None, None]:
        """
        Match text against defined patterns and pass the named groups of a match
        to the pattern's function, yielding the resulting object if there is one.
        
        An error can optionally be handled in two different ways, using `handle_error`.
            return:  End the search upon missing token
              skip:  Move cursor along by 1 and try matching again
        """

        i = 0
        while i < len(text):
            # Find the first matching pattern
            for p in self._patterns:
                if m := p.regex.match(text[i:]):
                    # Process match groups
                    token = p.function(**m.groupdict())

                    if token is not None:
                        yield token

                    # Move along to match the next token
                    i += len(m.group(0))
                    break
            
            else:
                if handle_error == 'return':
                    return

                if handle_error == 'skip':
                    i += 1
                    continue

                raise SyntaxError(f'No token could be matched at {i}')